package com.vtinfo.price.timing;

import com.vtinfo.core.services.dao.ApplicationContextProvider;
import com.vtinfo.core.services.dao.BaseDAO;
import com.vtinfo.core.services.dao.View;
import com.vtinfo.core.services.dao.ViewElf;
import com.vtinfo.core.services.dto.RecordUpdateTO;
import com.vtinfo.core.services.security.dto.DataRunnerBean;

public class TimingDAO extends BaseDAO {

    private View timingView = ViewElf.getViewFromTemplate("sql/ControllerTimingDAO.json", ApplicationContextProvider.getApplicationContext());

    public Long insertTiming(Timing timeRecord, DataRunnerBean dataRunnerBean){

        //is this the right place to do this? We have the app context here though.
        timeRecord.setSqlServer(ApplicationContextProvider.getApplicationContext().getEnvironment().getActiveProfiles()[0]);

        RecordUpdateTO ruTO = new RecordUpdateTO();
        ruTO.buildFromObjectLowerCamelTO(timeRecord);

        return basicCreateV1(Integer.parseInt(timeRecord.getCompany()), timingView, ruTO, dataRunnerBean);

    }

}
