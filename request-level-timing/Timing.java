package com.vtinfo.price.timing;

import java.io.Serializable;

public class Timing implements Serializable {
    private String company;
    private String endpoint;
    private Integer duration;
    private String sqlServer;
    private String action;
    private String market;
    private String supplier;
    private String brand;
    private Integer dataRowsReturned;


    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public Integer getDuration() {
        return this.duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getSqlServer() {
        return this.sqlServer;
    }

    public void setSqlServer(String sqlServer) {
        this.sqlServer = sqlServer;
    }

    public String getMarket() {
        return this.market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getSupplier() {
        return this.supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getDataRowsReturned() {
        return this.dataRowsReturned;
    }

    public void setDataRowsReturned(Integer dataRowsReturned) {
        this.dataRowsReturned = dataRowsReturned;
    }

}


