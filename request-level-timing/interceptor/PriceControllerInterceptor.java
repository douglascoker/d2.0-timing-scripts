package com.vtinfo.price.util.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.vtinfo.core.services.security.dto.DataRunnerBean;
import com.vtinfo.price.bo.security.SecurityElf;
import com.vtinfo.price.bo.security.UserFunction;
import com.vtinfo.price.bo.security.UserOperation;
import com.vtinfo.price.dao.datasource.DAOFactory;
import com.vtinfo.price.timing.Timing;
import com.vtinfo.price.timing.TimingDAO;
import net.bull.javamelody.internal.web.CounterServletResponseWrapper;
import org.apache.commons.io.IOUtils;
import org.joda.time.Instant;
import org.joda.time.Interval;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Component("PriceControllerInterceptor")
public class PriceControllerInterceptor
        extends HandlerInterceptorAdapter {

    HashMap<Integer, Instant> outstandingRequests = new HashMap<>();
    HashMap<Integer, Map> bodyJson = new HashMap<>();

    @Override
    public boolean preHandle (
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) {
        outstandingRequests.put(request.hashCode(), new Instant());
        try {
            String reqBody = IOUtils.toString(request.getReader());
            ObjectMapper mapper = new ObjectMapper();
            MapType type = mapper.getTypeFactory().constructMapType(
                    Map.class, String.class, Object.class);
            Map<String, Object> data = mapper.readValue(reqBody, type);
            bodyJson.put(request.hashCode(), data);
        } catch (Exception e) {
        }
        return true;
    }

    @Override
    public void afterCompletion(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            Exception ex) throws Exception {
        Instant startTime;
        if ((startTime = outstandingRequests.get(request.hashCode())) != null) {
            Interval runtime = new Interval(startTime, new Instant());
            Map data = bodyJson.get(request.hashCode());
            // this is because javamelody extends our already extended response wrapper, so we have to
            // cast it first to their type then grab an output stream of our type off it.
            String outData = new String(((ServletOutputStreamCopier) ((CounterServletResponseWrapper) response).getResponse().getOutputStream()).getCopy());

            ObjectMapper mapper = new ObjectMapper();
            MapType type = mapper.getTypeFactory().constructMapType(
                    Map.class, String.class, Object.class);
            Map<String, Object> outDataJson = mapper.readValue(outData, type);

            String action = "";
            if (data != null) {
                if (data.get("submitVersion") != null && data.get("submitVersion").equals("true")) {
                    action = " Submit For Approval";
                }
                else if (data.get("direction") != null && data.get("direction").equals("NEXT")) {
                        if (data.get("submitNotes") != null &&
                                ((String) data.get("submitNotes")).toLowerCase().contains("approve")) {
                            action = " Approve";
                        } else {
                            action = " Next";
                        }
                }
                else if (data.get("direction") != null && data.get("direction").equals("PREVIOUS")) {
                    action = " Reject";
                } else if (data.get("modelItemGroups") != null && ((ArrayList) data.get("modelItemGroups")).size() > 0) {
                    action = " Update";
                } else if (data.get("modelAccountDiscounts") != null && ((HashMap) data.get("modelAccountDiscounts")).size() > 0) {
                    action = " Update Account Discount";
                }

            }

            String market = "";
            String supplier = "";
            String brand = "";
            Integer dataRowsReturned = 1;
            try {

            if (outDataJson != null) {

                Map dataMap;
                if (outDataJson.get("data").getClass() == LinkedHashMap.class && (dataMap = (Map) outDataJson.get("data")) != null) {
                    dataRowsReturned = dataMap.size();
                    //update data structure
                    market = (dataMap.get("marketName") != null) ? (String) dataMap.get("marketName") : "";
                    supplier = (dataMap.get("supplierName") != null) ? (String) dataMap.get("supplierName") : "";
                    brand = (dataMap.get("brandShort") != null) ? (String) dataMap.get("brandShort") : "";

                    //get data structure
                    Map detailMap;
                    if ((detailMap = (Map) dataMap.get("modelDetail")) != null) {
                        if (detailMap.get("model") != null) {
                          market = (String) ((Map) detailMap.get("model")).get("marketName");
                          supplier = (String) ((Map) detailMap.get("model")).get("supplierName");
                          brand = (String) ((Map) detailMap.get("model")).get("brandShort");
                        }
                    }
                }
                if (outDataJson.get("data").getClass() == ArrayList.class) {
                    // todo some sort of generic collection/list size instead of two checks?
                    dataRowsReturned = ((ArrayList) outDataJson.get("data")).size();
                }
            }
            String company = request.getServletPath().split("/")[3];
            String accessToken = request.getParameter("accessToken");

                DataRunnerBean dataRunnerBean = SecurityElf.authorizeUserFunctionOperation(accessToken,
                        Integer.parseInt(company), UserFunction.PRICING, UserOperation.UPDATE);
                Timing timing = createTiming(request, runtime, action, market, supplier, brand, dataRowsReturned);
                writeTimingToDb(timing, dataRunnerBean);
            } catch (Exception e) {
                String foo = "bar";
                //do nothing, we don't really care
            }
            outstandingRequests.remove(request.hashCode());
        }
    }

    Long writeTimingToDb (Timing timing, DataRunnerBean dataRunnerBean) {
        TimingDAO dao = (TimingDAO) DAOFactory.getBean(TimingDAO.class, "logging");
        return dao.insertTiming(timing, dataRunnerBean);
    }

    Timing createTiming (HttpServletRequest request, Interval runtime, String action, String market, String supplier,
                         String brand, Integer dataRowsReturned) {
        Timing timing = new Timing();
        timing.setAction(request.getMethod() + action);
        timing.setCompany(request.getServletPath().split("/")[3]);
        timing.setDuration(Math.round(runtime.toDurationMillis()));
        timing.setEndpoint(request.getServletPath());
        timing.setSqlServer("local");
        timing.setMarket(market);
        timing.setSupplier(supplier);
        timing.setBrand(brand);
        timing.setDataRowsReturned(dataRowsReturned);
        return timing;
    }
}
