import psycopg2
import sys

def stringContains(str, charsetString):
    chars = list(charsetString)
    for char in chars:
        if char not in str:
            return False
    return True

def stringContainsWord(string, word):
    try:
        return string.index(word) >= 0
    except:
        return False

#def writeToPostgres():

def lookbackFillInParents(lines, index, parentMethod, cur):
    sql = "UPDATE PERFORMANCE.simple_timing set parent_name = %s where timestamp = %s and parent_name = '' and method_name = %s"
    foundStart = False
    for revIdx, line in enumerate(reversed(lines[0:index])):
        if stringContainsWord(line, "entered") and stringContainsWord(line, parentMethod):
            foundStart = True
            startIndex = index - revIdx -1
            break
    if foundStart:
        for line in lines[startIndex: index]:
            if stringContains(line, "#") and not stringContainsWord(line, "entered"):
                try:
                    lineFromTimestamp = line[line.index("INFO ")+5:] if stringContainsWord(line, "INFO") else line[line.index("WARN ")+5:]
                    lineFromHash = line[line.index("#")+1:]
                    timestamp = lineFromTimestamp[:lineFromTimestamp.index(",")]
                    method = lineFromHash[:lineFromHash.index("(")]
                    if parentMethod.strip() != method.strip():
                        cur.execute(sql, (parentMethod, timestamp, method))
                except:
                    pass



#filesToCheck = ("C:/opt/tomcatlogs/price-services-8081-info.log", "C:/opt/tomcatlogs/price-services-8081-warn.log")
filesToCheck = (["C:/opt/tomcatlogs/price-services-8081-info.log"])

for fileHandle in filesToCheck:
    with open(fileHandle, 'r') as file:
        conn_string = "host='localhost' port='5432' dbname='price_sandbox' user='postgres' password='password'"
        conn = psycopg2.connect(conn_string)
        conn.autocommit = True
        cur = conn.cursor()
        sql = "INSERT INTO PERFORMANCE.simple_timing values(%s,%s,%s,%s,%s)"
        lines = file.readlines()
        for idx, line in enumerate(lines):
            if stringContains(line, "#") and not stringContainsWord(line, "entered"):
                try:
                    lineFromTimestamp = line[line.index("INFO ")+5:] if stringContainsWord(line, "INFO") else line[line.index("WARN ")+5:]
                    lineFromHash = line[line.index("#")+1:]
                    lineFromTiming = lineFromHash[lineFromHash.index("in ")+3:]
                    timestamp = lineFromTimestamp[:lineFromTimestamp.index(",")]
                    time = lineFromTiming[:lineFromTiming.index(".")+3]
                    suffix = lineFromTiming[lineFromTiming.index(".")+3:]
                    method = lineFromHash[:lineFromHash.index("(")]
                    numTime = float(time)
                    suffix = suffix.replace("\n", "")
                    if stringContainsWord(suffix, "min") or suffix == "min":
                        numTime = round(numTime*60000)
                    if stringContainsWord(suffix, "s ") or suffix == "s":
                        numTime = round(numTime*1000)
                    if stringContainsWord(suffix, "ms"):
                        numTime = round(numTime)
                    if stringContainsWord(suffix, "µs"):
                        numTime = round(numTime/1000)
                    cur.execute(sql, (method, '', timestamp, numTime ,''))
                    if numTime > 1000: #note, possible to set granularity here, higher numbers mean only long-running functions are eligible to be parents
                        lookbackFillInParents(lines, idx, method, cur)
                    conn.commit()
                    print(timestamp)
                    print(method)
                    print(numTime)
                    print(suffix)
                except Exception as e:
                    pass
                print(line)
        cur.close()
        conn.close()