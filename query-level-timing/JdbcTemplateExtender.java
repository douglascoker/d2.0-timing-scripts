package com.vtinfo.core.services.dao;

import org.joda.time.Instant;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.lang.Nullable;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.List;

public class JdbcTemplateExtender extends JdbcTemplate
{
    private static final Logger LOG = LoggerFactory.getLogger(DAOElf.class.getName());
    private Connection conn;
    private PreparedStatement insertStatement;

    JdbcTemplateExtender(DataSource dataSource) {
        super(dataSource);
    }

    public int update(String sql, @Nullable Object... args) {
        String stringArgs = getSqlParamString((List<SqlParameterValue>)(Object)Arrays.asList(args));
        Instant start = new Instant();
        int res = super.update(sql, args);
        if (!sql.contains("controller_timing")) createDBRecord(sql, stringArgs, start, true);
        return res;
    }

    public <T> T queryForObject(String sql, @Nullable Object[] args, RowMapper<T> rowMapper) {
        String stringArgs = getSqlParamString((List<SqlParameterValue>)(Object)Arrays.asList(args));
        Instant start = new Instant();
        T res = super.queryForObject(sql, args, rowMapper);
        Boolean isUpdate = sql.contains("update ") || sql.contains("UPDATE ") || sql.contains("INSERT ") || sql.contains("insert ");
        if (!sql.contains("controller_timing")) createDBRecord(sql, stringArgs, start, isUpdate);
        return res;
    }

    public <T> List<T> query(String sql, @Nullable Object[] args, RowMapper<T> rowMapper) {
        String stringArgs = getSqlParamString((List<SqlParameterValue>)(Object)Arrays.asList(args));
        Instant start = new Instant();
        List<T> res = super.query(sql, args, rowMapper);
        if (!sql.contains("controller_timing")) createDBRecord(sql, stringArgs, start, false);
        return res;
    }

    private void createDBRecord(String statement, String stringArgs, Instant start, Boolean isUpdate) {
        String insertSql = "INSERT INTO performance.sql_timing (environment, statement, vars, time, timestamp, parent_method, is_write, action ) VALUES (?,?,?,?,DEFAULT,?,?,?)";
        final String dbDriver = "org.postgresql.Driver";
        final String URL = "jdbc:postgresql://localhost:5432/price_sandbox?currentSchema=performance&socketTimeout=300";
        final String USER = "postgres";
        final String PASS = "password";
        String env = ApplicationContextProvider.getApplicationContext().getEnvironment().getActiveProfiles()[0];
        String probableAction = getProbableSqlAction(statement);
        Interval runtime = new Interval(start, new Instant());

        try {
            if (this.conn == null || this.insertStatement == null) {
                this.conn = DriverManager.getConnection(URL, USER, PASS);
                this.insertStatement = conn.prepareStatement(insertSql);
            }
            this.insertStatement.setString(1, env);
            this.insertStatement.setString(2, statement);
            this.insertStatement.setLong(4, runtime.toDurationMillis());
            this.insertStatement.setString(3, stringArgs);
            this.insertStatement.setString(5, DAOElf.getCurrentMethod());
            this.insertStatement.setBoolean(6, isUpdate);
            this.insertStatement.setString(7, probableAction);
            this.insertStatement.execute();
        } catch (Exception e) {
            String foo = "bar";
            //do nada
        }

    }

    private String getSqlParamString (List<SqlParameterValue> sqlParams) {
        StringBuilder logSB = new StringBuilder();
        String fieldName;
        String fieldValue;
        for (SqlParameterValue field : sqlParams) {
            fieldName = field.getName();
            if (fieldName == null || fieldName.equals("?")) {
                fieldName = "";//not important to see a bunch of question marks
            } else {
                fieldName += ": ";
            }

            fieldValue = (field.getValue() == null) ? "null" : field.getValue().toString();
            logSB.append("[").append(fieldName).append(field.getValue()).append("]");
        }
        return logSB.toString();
    }

    String getProbableSqlAction(String sqlText) {
        String[] sqlArray = sqlText.split(" ");
        for (String word: sqlArray) {
            if (word.toLowerCase().equals("select")) {
                return "SELECT";
            }
            if (word.toLowerCase().equals("update")) {
                return "UPDATE";
            }
            if (word.toLowerCase().equals("insert")) {
                return "INSERT";
            }
            if (word.toLowerCase().equals("delete")) {
                return "DELETE";
            }
        }
        return "";
    }

}
