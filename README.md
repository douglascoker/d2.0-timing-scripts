**D2.0 Timing Methodology**
---

#### Controller-Level Timing

1. Plugged into existing dao logic to create a timing DAO/table in the price schema *If I had to do this again, probably wouldn't take this approach, 
but it was a good way to see how the DAO architecture worked. 
    * Created an end to end controller timer w DAO, all code/files for implementation in request-level-timing dir. 

2. Tapped into spring's request handing workings to get both requests (easy) and request bodies (harder) off incoming HTTP reqs. 
The only real complexity here was overriding Spring's request handling to allow multiple request body reads (ended up extending the class and just stowing the req body as a property). 
Then, created middleware to filter all incoming requests, time their executing, and write to a table. All code in request-level-timing/interceptor.

---

#### Method-Level Timing

1. Used existing libraries provided by JCABI for simply adding a @loggable annotation to classes, setup instructions [here](https://aspects.jcabi.com/annotation-loggable.html).
Really, this just involved adding a new maven dependancy, no hiccups noted. However, this implementation of AspectJ was fairly brittle, in that the logged output was predefined 
with only minimal configs possible. To avoid this, it seems fairly easy to implement a custom version [along these lines](https://stackoverflow.com/questions/8839077/how-to-use-aop-with-aspectj-for-logging),
but as we were short on time, the out of the box sufficed here. 
2. In order to get usable, filterable data from the logs that JCABI @Loggable generated there is a python log parser, found at method-level-timing/log-parser.py. 
As would be expected, there are some hardcoded configs in there, and the whole thing might be too brittle to ever reuse in any way. It's really only scoped to parsing these specific logs. 
3. Finally, and somewhat ashamedly, there's the JS (or more correctly, Google Apps Script) script that was a work in progress to get these parsed logs from the DB to a hierarchical spreadsheet. 
This code is a real mess, but it seemed to work in it's own special way. The rollup.sql file is used to rollup and dump the data to a google sheet, where the method-transversal.js script then attempted to
arrange it (and output JSON, so that other display strategies can be used).  If there's a real need to repurpose this, best to contact Doug at douglas.coker@vtinfo.com for a more detailed runthrough.


#### Query-Level Timing

1. Extended the existing Spring JDBC template in core services to log all executing queries to a table. This was pretty simple really, after creating the extended template class, the instances in Core-Services/
BaseDAO were edited to use the new extended template. To the client, no functionality changed, so there were no issues here. There is some (slow) string processing in the extended template, so there would be some 
reworking needed if this were to be production-ready code.
2. Extended template in the query-level-timing directory.


---
