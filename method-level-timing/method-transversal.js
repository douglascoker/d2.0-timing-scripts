

function insertIntoObject(obj, parentMethod, currentMethod, time) {
    var written = false;
    for (var node in obj) {
        if (node === parentMethod) {
            //already has a parent
            if (Array.isArray(obj[node])) {
                obj[node].push(currentMethod);
                written = true;
            }
        }
        if (Array.isArray(obj[node])) {
            obj[node].forEach(function(listEl, arrIdx) {
                if (typeof listEl == 'object') {
                    insertIntoObject(listEl, parentMethod, currentMethod);
                } else if (listEl == parentMethod) {
                    obj[node][arrIdx] = {};
                    obj[node][arrIdx][parentMethod] = [currentMethod];
                    written = true;
                }
            })
        }
    }
}

function getDataPosition(range, searchString) {
    var values = range.getValues();
    var pos = [1,1];
    var columnIdx = 1;
    var rowIdx = 1;
    values.forEach(function(valRow) {
        columnIdx = 1;
        valRow.forEach(function(val){
            if (val === searchString){
                pos[0] = rowIdx;
                pos[1] = columnIdx;
            }
            columnIdx++
        });
        rowIdx++
    });
    //Logger.log(pos);
    return pos;
}

function getStartIndex(dataRow) {
    var startIdx = 0;
    dataRow.forEach(function(datum){
        if (datum !== '') {
            return startIdx;
        }
        startIdx++
    });
    return startIdx;
}

function getObjectCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function startTraverse() {
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = doc.getSheets()[0];
    var sheet2 = doc.getSheets()[1];
    var sheet3 = doc.getSheets()[2];
    var data = sheet.getDataRange().getValues();
    var jsonObj = {};
    jsonObj["updatePriceModelStatusV1"] = [];
    traverseNode("updatePriceModelStatusV1", data, jsonObj);
    sheet3.getRange(1,1).setValue([JSON.stringify(jsonObj)]);
}

function traverseNode(nodeValue, data, jsonObj) {
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    var sheet2 = doc.getSheets()[1];
    var traverseList = [];
    var written = false;
    var dataCopy = getObjectCopy(data);
    getObjectCopy(data).forEach(function (datum, idx)
    {
        var startIdx = getStartIndex(datum);
        Logger.log(startIdx);
        //aka, we've found a child (parent = parent)
        if (datum[startIdx+1] === nodeValue) {
            //push the child onto the traverse list
            traverseList.push(datum[startIdx]);
            //get the position of the current node on the results sheet (or 1, 1 if not present)
            var pos = getDataPosition(sheet2.getRange(1,1,120,150), nodeValue);
            Logger.log(pos);
            //new row right afterwards
            sheet2.insertRowAfter(pos[0]);
            SpreadsheetApp.flush();
            //set the child value in the new row, one column over + " (" + datum[startIdx+2] + "ms)"
            sheet2.getRange(pos[0]+1, pos[1]+1).setValue([datum[startIdx]]);
            insertIntoObject(jsonObj, nodeValue, datum[startIdx]);
            SpreadsheetApp.flush();
            //set timing
            sheet2.getRange(pos[0]+1, pos[1]+2).setValue([datum[startIdx+2]]);
            SpreadsheetApp.flush();
            written = true;
            dataCopy.splice(idx, 1);
            traverseNode(datum[startIdx], dataCopy, jsonObj);
            //sheet2.getRange(row+1, 1).setValue([data[i][0]])
        }
    });
}
