select method_name, parent_name, round(sum(time)) as time_spent, count(timestamp) as times_executed
from (select method_name, parent_name, time, timestamp from performance.simple_timing) innerSel
group by method_name, parent_name
having round(sum(time))>1000
;