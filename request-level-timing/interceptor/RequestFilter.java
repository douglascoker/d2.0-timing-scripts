package com.vtinfo.price.util.interceptor;

import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RequestFilter implements Filter {

    @Override
    public void destroy() {
        // just cause it's abstract..
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // again, keeping the compiler happy
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if(servletRequest instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            //filterChain.doFilter(new RequestWrapper(request), new ContentCachingResponseWrapper(response));
            //filterChain.doFilter(new RequestWrapper(request), response);
            filterChain.doFilter(new RequestWrapper(request), new ResponseWrapper(response));
        }
    }
}
